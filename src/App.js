const gDevcampReact = {
  title: 'Chào mừng đến với Devcamp React',
  image: 'https://reactjs.org/logo-og.png',
  benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
  studyingStudents: 20,
  totalStudents: 100
}
const tyLeSinhVienTheoHoc = () => {
  return gDevcampReact.studyingStudents / gDevcampReact.totalStudents *100
}
function App() {
  return (
    <div className="App">

     <h1>{gDevcampReact.title}</h1>

     <img src={gDevcampReact.image} width="500px"/>
     
     <p> tỷ lệ sinh viên theo học:  {tyLeSinhVienTheoHoc()} %</p>
     {tyLeSinhVienTheoHoc() > 15 ? "giatridung : sinh viên đăng kí học nhiều" : "giatrisai : : sinh viên đăng kí học ít"} 
   <ul>
    {
      gDevcampReact.benefits.map((value,index) =>{
        return <li key={index}>{value}</li>
      })
    }
   </ul>
    </div>
  );
}

export default App;
